import Vue from 'vue'

const isDev = require('electron-is-dev')
if (isDev) {
    Vue.config.productionTip = false
}

import App from './App.vue'
import './quasar'
import {ipcRenderer} from 'electron'


ipcRenderer.on('channel', () => {
    // eslint-disable-next-line no-console
    console.log('function from index.ts')
})

// const fs = require('fs')
// const root = fs.readdirSync('/')
//
// console.log(root)


new Vue({
    render: h => h(App),
}).$mount('#app')
