import Vue from 'vue'

import './styles/quasar.sass'
// // @ts-ignore
// import {default as lang} from 'quasar/lang/ru.js'

// @ts-ignore
import {default as lang} from 'quasar/dist/lang/ru.umd.min'


import {Quasar} from 'quasar'
import {components} from "./quasar-components"
// import {components} from "src/renderer/quasar-components";


// const components = require('quasar/src/components')

// import * as components from './components.js'


Vue.use(Quasar, {
    config: {},
    // components: { QLayout},
    components,
    directives: { /* not needed if importStrategy is not 'manual' */},
    plugins: {},
    lang: lang
})

//     ,
//     "plugins": [
//   [
//     "transform-imports",
//     {
//       "quasar": {
//         "transform": "quasar/dist/babel-transforms/imports.js",
//         "preventFullImport": true
//       }
//     }
//   ]
// ]
