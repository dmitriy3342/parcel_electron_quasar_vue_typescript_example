const {format} = require('url')
import {app, protocol, BrowserWindow, ipcMain} from 'electron'

// import installExtension, {VUEJS_DEVTOOLS} from 'electron-devtools-installer'
const isDev = require('electron-is-dev')
const {resolve} = require('app-root-path')

console.log('main.ts')

// import {
//     createProtocol,
//     installVueDevtools
// } from 'vue-cli-plugin-electron-builder/lib'

// const isDev = process.env.NODE_ENV ? !process.env.NODE_ENV.match(/production/) : true

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win: BrowserWindow | null

// Scheme must be registered before the app is ready
// protocol.registerSchemesAsPrivileged([{scheme: 'app', privileges: {secure: true, standard: true}}])

//
ipcMain.on('channel', (event, args) => {

    // eslint-disable-next-line no-console
    console.log('channel_main on from Background.ts')

    event.reply('channel', 'value')
})


function createWindow() {
    console.log('create window')
    // Create the browser window.
    win = new BrowserWindow({
        width: 800, height: 600, webPreferences: {
            nodeIntegration: true,
            nodeIntegrationInWorker: true
        }
    })


    const devPath = format({
        pathname: resolve('.app\\renderer\\development\\index.html'),
        protocol: 'file:',
        slashes: true
    })
    const prodPath = format({
        pathname: resolve('.app\\renderer\\production\\index.html'),
        protocol: 'file:',
        slashes: true
    })
    const url = isDev ? devPath : prodPath

    win.loadURL(url)


    if(isDev){
        win.webContents.openDevTools({mode: 'bottom'})
    }


    // window.loadURL(`file://${__dirname}/index.jade`)


    // 1124 win.loadFile('index.html')
    // if (process.env.WEBPACK_DEV_SERVER_URL) {
    //     // Load the url of the dev server if in development mode
    //     win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    //     if (!process.env.IS_TEST) win.webContents.openDevTools()
    // } else {
    //     // createProtocol('app')
    //     // Load the index.html when not in development
    //     win.loadURL('app://./index.html')
    // }

    win.on('closed', () => {
        win = null
    })
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    console.log('window-all-closed')
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    console.log('activate')
    if (win === null) {
        createWindow()
    }
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
    console.log('ready')

    // if (isDev && !process.env.IS_TEST) {
    //     // Install Vue Devtools
    //     // Devtools extensions are broken in Electron 6.0.0 and greater
    //     // See https://github.com/nklayman/vue-cli-plugin-electron-builder/issues/378 for more info
    //     // Electron will not launch with Devtools extensions installed on Windows 10 with dark mode
    //     // If you are not using Windows 10 dark mode, you may uncomment these lines
    //     // In addition, if the linked issue is closed, you can upgrade electron and uncomment these lines
    //     try {
    //       await installVueDevtools()
    //     } catch (e) {
    //       console.error('Vue Devtools failed to install:', e.toString())
    //     }
    //
    // }

    // if (session.defaultSession) {
    //     session.defaultSession.webRequest.onBeforeRequest((details, callback) => {
    //         const redirectURL = details.url.replace(/^devtools:\/\/devtools\/remote\/serve_file\/@[0-9a-f]{40}/, "https://chrome-devtools-frontend.appspot.com/serve_file/@675968a8c657a3bd9c1c2c20c5d2935577bbc5e6");
    //         if (redirectURL !== details.url) {
    //             callback({redirectURL});
    //         } else {
    //             callback({});
    //         }
    //     })
    // }

    // if (win === null) {
    createWindow();

    // if (isDev) {
    //     await installExtension(VUEJS_DEVTOOLS)
    //     if (win) {
    //         win.webContents.openDevTools({mode: 'bottom'})
    //     }
    // }

    // if (win !== null) {
    //     win.webContents.session.webRequest.onBeforeRequest(
    //         {urls: ['devtools://devtools/remote/*']},
    //         (details, callback) => {
    //             callback({
    //                 redirectURL: details.url.replace('devtools://devtools/remote/', 'https://chrome-devtools-frontend.appspot.com/')
    //             });
    //         }
    //     );
    // }
    // }else{
    // }

})

// Exit cleanly on request from parent process in development mode.
if (isDev) {
    process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = 'true';
    if (process.platform === 'win32') {
        process.on('message', data => {
            if (data === 'graceful-exit') {
                app.quit()
            }
        })
    } else {
        process.on('SIGTERM', () => {
            app.quit()
        })
    }
}




